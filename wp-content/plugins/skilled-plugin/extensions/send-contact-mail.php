<?php

function send_mail()
{
    $formData = $_POST['form_data'];

    $area = isset($formData['area']) && $formData['area'] > 0 ? get_term_by("id", $formData['area'], 'course-category')->name : '';
    $carrera = (isset($formData['carrera']) && $formData['carrera'] > 0) ? (get_the_title($formData['carrera'])) : "";
    $curso = (isset($formData['curso']) && $formData['curso'] > 0) ? (get_the_title($formData['curso'])) : "";

    $asunto = "Contacto desde web";

    $headers[] = 'From: ' . $formData["nombre"] .' <' . $formData['email'] . '>';
    $headers[] = 'Content-Type: text/html; charset=UTF-8';

    $message = "";

    if (strlen($area) > 0) $message += "Area: " . $area . "<br>";
    if (strlen($carrera) > 0) $message += "Carrera: " . $carrera . "<br>";
    if (strlen($curso) > 0) $message += "Curso: " . $curso . "<br>";

    $message =+ "<br>"
        . "Nombre: " .  $formData['nombre'] . "<br>"
        . "Email: " .  $formData['email'] . "<br>"
        . "Asunto: " .  $formData['asunto'] . "<br>"
        . "<br>"
        . "Mensaje: " . $formData['mensaje'] . "<br>"
        . "" . "<br>";

    add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
    wp_mail( get_option('admin_email'), $asunto, $message, $headers );

    $course_video_embed = null;

    if ((isset($formData['carrera']) && $formData['carrera'] > 0)) {
        $course_video_embed = get_post_meta( $formData['carrera'], '_course_video_embed', true );
        $asunto = "Información sobre carrera de " . $carrera;
    } else if ((isset($formData['curso']) && $formData['curso'] > 0)) {
        $course_video_embed = get_post_meta( $formData['curso'], '_course_video_embed', true );
        $asunto = "Información sobre curso de " . $curso;
    }

    if (!is_null($course_video_embed)) {
        $headers = array();
        $headers[] = 'From: ' . 'Secretaría - Integral Instituto Superior de Diseño' .' <' . 'secretaria@integral.edu.ar' . '>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
        wp_mail( $formData['email'], $asunto, html_entity_decode($course_video_embed), $headers);
    }
}

function send_mail_course () {
    $formData = $_POST['form_data'];

    $area = isset($formData['area']) && $formData['area'] > 0 ? get_term_by("id", $formData['area'], 'course-category')->name : '';
    $carrera = (isset($formData['carrera']) && $formData['carrera'] > 0) ? (get_the_title($formData['carrera'])) : "";
    $curso = (isset($formData['curso']) && $formData['curso'] > 0) ? (get_the_title($formData['curso'])) : "";

    $asunto = "Solicitud de inscripción";

    $headers[] = 'From: ' . $formData["nombre"] .' <' . $formData['email'] . '>';
    $headers[] = 'Content-Type: text/html; charset=UTF-8';

    $message = "";

    if (strlen($area) > 0) $message += "Area: " . $area . "<br>";
    if (strlen($carrera) > 0) $message += "Carrera: " . $carrera . "<br>";
    if (strlen($curso) > 0) $message += "Curso: " . $curso . "<br>";

    $message =+ "<br>"
        . "Nombre: " .  $formData['nombre'] . "<br>"
        . "Email: " .  $formData['email'] . "<br>"
        . "Asunto: " .  $asunto . "<br>"
        . "<br>"
        . "Mensaje: " . $formData['mensaje'] . "<br>"
        . "" . "<br>";

    add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
    wp_mail( get_option('admin_email'), $asunto, $message, $headers );
}

add_action('wp_ajax_send_integral_contact_mail', 'send_mail');
add_action('wp_ajax_nopriv_send_integral_contact_mail', 'send_mail');

add_action('wp_ajax_send_integral_course_mail', 'send_mail_course');
add_action('wp_ajax_nopriv_send_integral_course_mail', 'send_mail_course');