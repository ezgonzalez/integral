<?php

if ( ! function_exists( 'Sensei' ) ) {
	return;
}

$search_page      = skilled_get_option('sensei-course-search-page', false);
$search_page_link = $search_page ? get_permalink( $search_page ) : site_url( '/' );

$args = array(
	'taxonomy'        => 'course-category',
	'name'            => 'course-category',
	'show_option_all' => 'Categoría',
	'show_count'      => true,
	'hierarchical'    => true,
	'hide_empty'	  => false
);

if ( skilled_get_option('is-rtl', false) ) {
	$args['show_count'] = false;
}

if ( isset( $_GET['course-category'] ) ) {
	$args['selected'] = $_GET['course-category'];
}
?>

<?php
$argsTypes = array(
	'taxonomy'        => 'course-type',
	'name'            => 'course-type',
	'show_option_all' => 'Tipo',
	'show_count'      => true,
	'hierarchical'    => true,
	'hide_empty'	  => false,
	'show_count'      => false
);

if ( isset( $_GET['course-type'] ) ) {
	$argsTypes['selected'] = $_GET['course-type'];
}

$argsModality = array(
	'taxonomy'        => 'course-modality',
	'name'            => 'course-modality',
	'show_option_all' => 'Modalidad',
	'show_count'      => true,
	'hierarchical'    => true,
	'hide_empty'	  => false,
	'show_count'      => false
);

if ( isset( $_GET['course-modality'] ) ) {
	$argsModality['selected'] = $_GET['course-modality'];
}

?>

<form action="<?php echo esc_url( $search_page_link ); ?>" method="get" id="searchform" class="search-form-wrap search-for-courses">
	<input type="hidden" name="search-type" value="courses"/>
	<ul>
		<li>
			<?php wp_dropdown_categories( $args ); ?>
		</li>
		<li>
			<?php wp_dropdown_categories($argsTypes); ?>
		</li>
		<li>
			<?php wp_dropdown_categories($argsModality); ?>
		</li>
		<li>
			<input type="text" value="<?php if ( skilled_is_search_courses() ) {
				echo get_search_query();
			} ?>" name="s" placeholder="<?php esc_html_e( 'Type Keyword', 'skilled' ); ?>"/>
		</li>
		<li class="search-courses-button-item">
			<button type="submit" class="wh-button"><?php esc_html_e( 'Search', 'skilled' ); ?></button>
		</li>
	</ul>
</form>