<?php
if ( post_password_required() ) {
	return;
}

if ( have_comments() ) : ?>
	<section id="comments">
		<h3><?php printf( _n( 'Una respuesta a &ldquo;%2$s&rdquo;', '%1$s respuestas a &ldquo;%2$s&rdquo;', get_comments_number(), 'skilled' ), number_format_i18n( get_comments_number() ), get_the_title() ); ?></h3>
		<hr class="wh-separator" />
		<ul class="comment-list">
			<?php wp_list_comments( array( 'walker' => new Wheels_Walker_Comment ) ); ?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav>
				<ul class="pager">
					<?php if ( get_previous_comments_link() ) : ?>
						<li class="previous"><?php previous_comments_link( esc_html__( '&larr; Comentarios más antiguos', 'skilled' ) ); ?></li>
					<?php endif; ?>
					<?php if ( get_next_comments_link() ) : ?>
						<li class="next"><?php next_comments_link( esc_html__( 'Comentarios más recientes &rarr;', 'skilled' ) ); ?></li>
					<?php endif; ?>
				</ul>
			</nav>
		<?php endif; ?>

		<?php if ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
			<div class="alert alert-warning">
				<?php _e( 'Comentarios cerrados.', 'skilled' ); ?>
			</div>
		<?php endif; ?>
	</section><!-- /#comments -->
<?php endif; ?>

<?php if ( ! have_comments() && ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
	<section id="comments">
		<div class="alert alert-warning">
			<?php _e( 'Comentarios cerrados.', 'skilled' ); ?>
		</div>
	</section><!-- /#comments -->
<?php endif; ?>

<?php if ( comments_open() ) : ?>
	<section id="respond">
		<h3><?php comment_form_title( esc_html__( 'Dejar un comentario', 'skilled' ), esc_html__( 'Dejar un comentario para %s', 'skilled' ) ); ?></h3>
		<hr class="wh-separator">
		<p class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></p>
		<?php if ( get_option( 'comment_registration' ) && ! is_user_logged_in() ) : ?>
			<p><?php printf( __( 'Necesitás <a href="%s">iniciar sesión</a> para comentar.', 'skilled' ), wp_login_url( get_permalink() ) ); ?></p>
		<?php else : ?>
			<form action="<?php echo get_option( 'siteurl' ); ?>/wp-comments-post.php" method="post" id="commentform">
				<?php if ( is_user_logged_in() ) : ?>
					<p>
						<?php printf( __( 'Iniciaste sesión como <a href="%s/wp-admin/profile.php">%s</a>.', 'skilled' ), get_option( 'siteurl' ), $user_identity ); ?>
						<a href="<?php echo wp_logout_url( get_permalink() ); ?>"
						   title="<?php esc_html_e( 'Salir de esta cuenta', 'skilled' ); ?>"><?php esc_html_e( 'Cerrar sesión &raquo;', 'skilled' ); ?></a>
					</p>
				<?php else : ?>
					<div class="form-group">
						<label for="author"><?php esc_html_e( 'Nombre', 'skilled' );
							if ( $req ) {
								esc_html_e( ' (obligatorio)', 'skilled' );
							} ?></label>
						<input type="text" class="form-control" name="author" id="author"
						       value="<?php echo esc_attr( $comment_author ); ?>" size="22" <?php if ( $req ) {
							echo 'aria-required="true"';
						} ?>>
					</div>
					<div class="form-group">
						<label for="email"><?php esc_html_e( 'Email (no se publicará)', 'skilled' );
							if ( $req ) {
								esc_html_e( ' (obligatorio)', 'skilled' );
							} ?></label>
						<input type="email" class="form-control" name="email" id="email"
						       value="<?php echo esc_attr( $comment_author_email ); ?>" size="22" <?php if ( $req ) {
							echo 'aria-required="true"';
						} ?>>
					</div>
					<!-- <div class="form-group">
						<label for="url"><?php esc_html_e( 'Website', 'skilled' ); ?></label>
						<input type="url" class="form-control" name="url" id="url"
						       value="<?php echo esc_attr( $comment_author_url ); ?>" size="22">
					</div> -->
				<?php endif; ?>
				<div class="form-group">
					<label for="comment"><?php esc_html_e( 'Comentario', 'skilled' ); ?></label>
					<textarea name="comment" id="comment" class="form-control" rows="5" aria-required="true"></textarea>
				</div>
				<p><input name="submit" class="btn btn-primary" type="submit" id="submit"
				          value="<?php esc_html_e( 'Enviar comentario', 'skilled' ); ?>"></p>
				<?php comment_id_fields(); ?>
				<?php do_action( 'comment_form', $post->ID ); ?>
			</form>
		<?php endif; ?>
	</section><!-- /#respond -->
<?php endif; ?>
