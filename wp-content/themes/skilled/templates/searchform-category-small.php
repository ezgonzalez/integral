<?php

if ( ! function_exists( 'Sensei' ) ) {
	return;
}

$search_page      = skilled_get_option('sensei-course-search-page', false);
$search_page_link = $search_page ? get_permalink( $search_page ) : site_url( '/' );

$args = array(
	'taxonomy'        => 'course-category',
	'name'            => 'course-category',
	'show_option_all' => 'Categoría',
	'show_count'      => true,
	'hierarchical'    => true,
	'hide_empty'	  => false
);

if ( skilled_get_option('is-rtl', false) ) {
	$args['show_count'] = false;
}

if ( isset( $_GET['course-category'] ) ) {
	$args['selected'] = $_GET['course-category'];
}
?>

<?php
$argsTypes = array(
	'taxonomy'        => 'course-type',
	'name'            => 'course-type',
	'show_option_all' => 'Tipo',
	'show_count'      => true,
	'hierarchical'    => true,
	'hide_empty'	  => false,
	'show_count'      => false
);

if ( isset( $_GET['course-type'] ) ) {
	$argsTypes['selected'] = $_GET['course-type'];
}

$argsModality = array(
	'taxonomy'        => 'course-modality',
	'name'            => 'course-modality',
	'show_option_all' => 'Modalidad',
	'show_count'      => true,
	'hierarchical'    => true,
	'hide_empty'	  => false,
	'show_count'      => false
);

if ( isset( $_GET['course-modality'] ) ) {
	$argsModality['selected'] = $_GET['course-modality'];
}

$cat_args=array(
  'orderby' => 'name',
  'order' => 'ASC',
  'taxonomy'        => 'course-type'
   );
$categories=get_categories($cat_args);
/*echo "<pre>";
print_r($categories);
echo '</pre>';*/
?>


<?php foreach ( $categories as $category ) { ?>
<form action="/area" method="get" id="searchform" class="search-form-wrap search-for-courses <?php echo $_GET['course-type'] == $category->term_id ? 'category-active' : ''; ?>">
	<input type="hidden" name="course-type" value="<?php echo $category->term_id; ?>"/>
	<button type="submit" class="wh-button"><?php echo $category->name; ?></button>
</form>
<?php } ?>
