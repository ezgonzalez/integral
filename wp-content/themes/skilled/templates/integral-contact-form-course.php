<?php
if ( ! function_exists( 'Sensei' ) ) {
    return;
}

$search_page      = skilled_get_option('sensei-course-search-page', false);
$search_page_link = $search_page ? get_permalink( $search_page ) : site_url( '/' );

$category = wp_get_post_terms(get_post()->ID, 'course-category')[0];
$courseTypeTerm = wp_get_post_terms(get_post()->ID, 'course-type')[0];

$args = array(
    'taxonomy'        => 'course-category',
    'name'            => 'area',
    'show_option_all' => 'Area',
    'selected'        => $category->term_id,
    'show_count'      => true,
    'hierarchical'    => true
);

if ( skilled_get_option('is-rtl', false) ) {
    $args['show_count'] = false;
}

if ( isset( $category ) ) {
    $args['value_field'] = $category->term_id;
}


$carrerasArgs = array(
    'post_type' => 'course',
    'numberposts' => -1,
    'show_option_all' => 'Carrera',
    'select_name' => 'carrera',
    'selected' => get_post()->ID,
    'class' => 'multiple-select',
    'tax_query' => array(
        array(
            'taxonomy' => 'course-type',
            'field' => 'slug',
            'terms' => 'carrera',
            'include_children' => false
        )
    )
);

$cursosArgs = array(
    'post_type' => 'course',
    'numberposts' => -1,
    'show_option_all' => 'Curso',
    'select_name' => 'curso',
    'value_field' => get_post()->ID,
    'class' => 'multiple-select',
    'tax_query' => array(
        array(
            'taxonomy' => 'course-type',
            'field' => 'slug',
            'terms' => 'curso',
            'include_children' => false
        )
    )
);

?>
<form action="<?php echo esc_url( $search_page_link ); ?>" method="get" id="contactform" class="search-form-wrap search-form-wrap-integral-course search-for-courses">
    <div class="search-form-wrap-integral-box">
        <div  class="search-form-wrap-integral-item">
            <ul>
                <li>
                    <label>Areas</label>
                    <?php wp_dropdown_categories( $args ); ?>
                </li>
            </ul>
        </div>

        <div  class="search-form-wrap-integral-item">
            <ul>
                <li>
                    <label>Nombre</label>
                    <input type="text" value="<?php if ( skilled_is_search_courses() ) {
                        echo get_search_query();
                    } ?>" name="nombre" placeholder="Nombre"/>
                </li>
                <li>
                    <label>Email</label>
                    <input type="email" value="<?php if ( skilled_is_search_courses() ) {
                        echo get_search_query();
                    } ?>" name="email" placeholder="Email"/>
                </li>
            </ul>
        </div>

        <div  class="search-form-wrap-integral-item">
            <ul>
              <li>
                  <label>Carreras</label>
                  <?php wp_dropdown_posts( $carrerasArgs ); ?>
              </li>
              <li>
                  <label>Cursos</label>
                  <?php wp_dropdown_posts( $cursosArgs ); ?>
              </li>
            </ul>
        </div>

        <div  class="search-form-wrap-integral-item">
            <ul>
                <li>
                    <label>Mensaje</label>
                    <textarea type="text" value="<?php if ( skilled_is_search_courses() ) {
                        echo get_search_query();
                    } ?>" name="mensaje" placeholder="Mensaje"></textarea>
                </li>
            </ul>
        </div>
    </div>

    <input type="hidden" name="search-type" value="courses"/>

    <div class="search-form-wrap-integral-box">
        <div  class="search-form-wrap-integral-item-100">
                    <button type="button" class="wh-button">Enviar</button>
        </div>
    </div>
</form>
<script>
    jQuery.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        jQuery.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    jQuery('#contactform button').click(function () {
        var formData = jQuery('#contactform').serializeObject();
        jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', { 'action': 'send_integral_course_mail', 'form_data': formData });
    });
</script>
