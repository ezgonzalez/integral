<?php
$logo_url = skilled_get_logo_url();

if (!is_front_page()) {
	$logo_url = "/wp-content/uploads/2015/10/logo_color.png";
}

?>
<?php if ( $logo_url ): ?>
	<div class="<?php echo skilled_class( 'logo' ); ?>">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<img src="<?php echo esc_url( $logo_url ); ?>" alt="logo">
		</a>
	</div>
<?php else: ?>
	<div class="<?php echo skilled_class( 'logo' ); ?>">
		<h1 class="site-title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
		</h1>

		<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
	</div>
<?php endif; ?>
