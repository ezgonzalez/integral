<?php
if ( ! function_exists( 'Sensei' ) ) {
    return;
}

$search_page      = skilled_get_option('sensei-course-search-page', false);
$search_page_link = $search_page ? get_permalink( $search_page ) : site_url( '/' );

$args = array(
    'taxonomy'        => 'course-category',
    'name'            => 'area',
    'show_option_all' => 'Area',
    'show_count'      => true,
    'hierarchical'    => true
);

if ( skilled_get_option('is-rtl', false) ) {
    $args['show_count'] = false;
}

if ( isset( $_GET['course-category'] ) ) {
    $args['selected'] = $_GET['course-category'];
}

$carrerasArgs = array(
    'post_type' => 'course',
    'numberposts' => -1,
    'show_option_all' => 'Carrera',
    'select_name' => 'carrera',
    'class' => 'multiple-select',
    'tax_query' => array(
        array(
            'taxonomy' => 'course-type',
            'field' => 'slug',
            'terms' => 'carrera',
            'include_children' => false
        )
    )
);

$cursosArgs = array(
    'post_type' => 'course',
    'numberposts' => -1,
    'show_option_all' => 'Curso',
    'select_name' => 'curso',
    'class' => 'multiple-select',
    'tax_query' => array(
        array(
            'taxonomy' => 'course-type',
            'field' => 'slug',
            'terms' => 'curso',
            'include_children' => false
        )
    )
);

?>
<form action="<?php echo esc_url( $search_page_link ); ?>" method="get" id="contactform" class="search-form-wrap search-form-wrap-integral search-for-courses">
    <div class="search-form-wrap-integral-box">
        <div  class="search-form-wrap-integral-item">
            <ul>
                <li>
                    <label>Areas</label>
                    <?php wp_dropdown_categories( $args ); ?>
                </li>
                <li>
                    <label>Carreras</label>
                    <?php wp_dropdown_posts( $carrerasArgs ); ?>
                </li>
            </ul>
        </div>

        <div  class="search-form-wrap-integral-item">
            <ul>
                <li>
                    <label>Nombre</label>
                    <input name="nombre" type="text" value="<?php if ( skilled_is_search_courses() ) {
                        echo get_search_query();
                    } ?>" placeholder="Nombre"/>
                </li>
                <li>
                    <label>Cursos</label>
                    <?php wp_dropdown_posts( $cursosArgs ); ?>
                </li>
            </ul>
        </div>

        <div  class="search-form-wrap-integral-item">
            <ul>
              <li>
                  <label>Email</label>
                  <input name="email" type="email" value="<?php if ( skilled_is_search_courses() ) {
                      echo get_search_query();
                  } ?>" placeholder="Email"/>
              </li>
                <li>
                    <label>Mensaje</label>
                    <textarea name="mensaje" type="text" value="<?php if ( skilled_is_search_courses() ) {
                        echo get_search_query();
                    } ?>" placeholder="Mensaje"></textarea>
                </li>
            </ul>
        </div>
    </div>

    <input type="hidden" name="search-type" value="courses"/>

    <div class="search-form-wrap-integral-box">
        <div  class="search-form-wrap-integral-item-100">
                    <button type="button" class="wh-button">Enviar</button>
        </div>
    </div>
</form>

<script>
    jQuery.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        jQuery.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    jQuery('#contactform button').click(function () {
        var formData = jQuery('#contactform').serializeObject();
        jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', { 'action': 'send_integral_contact_mail', 'form_data': formData });
    });
</script>
