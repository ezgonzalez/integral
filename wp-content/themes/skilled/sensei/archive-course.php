<?php

$search_page      = skilled_get_option('sensei-course-search-page', false);
$search_page_link = $search_page ? get_permalink( $search_page ) : site_url( '/' );


if (get_query_var('course-category')) {
    header("Location: " . $search_page_link . '?search-type=areas&course-category='. get_term_by('slug', get_query_var('course-category'), 'course-category')->term_id .'&status=&s=');
} elseif (get_query_var('course-type')) {
    header("Location: " . $search_page_link . '?search-type=courses&course-type='. get_term_by('slug', get_query_var('course-type'), 'course-type')->term_id .'&status=&s=');
} elseif (get_query_var('course-modality')) {
    header("Location: " . $search_page_link . '?search-type=courses&course-modality='. get_term_by('slug', get_query_var('course-modality'), 'course-modality')->term_id .'&status=&s=');
}

die();

?>